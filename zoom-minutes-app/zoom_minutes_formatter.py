import argparse
import re


fillers = re.compile(r"uh|Uh|ah|Ah|um|Um|uh,|Uh,|ah,|Ah,|um,|Um,")


def parse_args():
    parser = argparse.ArgumentParser(
        prog="Zoom Minutes Formatter",
        description="Formats minutes from Zoom",
        epilog="Copyright EESTEC IT Team",
    )

    parser.add_argument("-i", "--input", required=True)
    parser.add_argument("-o", "--output", required=True)
    return parser.parse_args()


def main(args):
    lines: list(str) = list()
    formatted_lines: list(str) = list()
    last_index = -1
    with open(args.input) as fin:
        lines = fin.readlines()
    with open(args.output, "w") as fout:
        for line in lines:
            l = re.sub(r"\s{2,}", " ", line.strip())
            if not re.search(r"\|", l):
                continue

            if len(formatted_lines) == 0:
                formatted_lines.append(l)
                last_index += 1

            # concat two speech text together if the speaker is the same ++
            # remove Um, um, ah, Ah fillers ++
            # get rid of multiple spaces ++
            if last_index != -1:
                prev_line = formatted_lines[last_index]
                prev_line_parts = prev_line.replace("\n", "").split(":")
                line_parts = l.replace("\n", "").split(":")

                if prev_line_parts[0] == line_parts[0]:
                    if len(line_parts) >= 2:
                        formatted_lines[
                            last_index
                        ] = f"{prev_line} {re.sub(fillers, '', line_parts[1])}"
                else:
                    formatted_lines.append(
                        f"\n{line_parts[0]} {re.sub(fillers, '', line_parts[1])}"
                    )
                    last_index += 1


if __name__ == "__main__":
    args = parse_args()
    main(args)
