# GeoJSON for EESTEC
Katea (LC Bucharest) used geojson to render the map on EESTEC.net/about

This folder contains some data and bits of python within an jupyter notebook that made that easier.

## Data
In the folder ```data``` you'll find a geojson representing Europe and an augmented json file, in which each country also has the number of branches as attribute and in which Israel is not included.

## The Code
Simple python to iterate through the data to filter and augment. GeoJSON can also be edited graphically with https://geoman.io/geojson-editor.